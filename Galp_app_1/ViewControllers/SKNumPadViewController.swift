//
//  SKNumPadViewController.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 05.05.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

private let reuseIdentifierNumPadCell = "SKNumPadCell"

class SKNumPadViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var numPadView: UIView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var numTitleLabel: UILabel!
    @IBOutlet weak var clearButton: UIButton!
    
    let arrNumPad = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "backspace", "0", "search"]
    var number = 0
    var onSearch:((_ data: String) -> ())?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let nibName = UINib(nibName: reuseIdentifierNumPadCell, bundle: nil)
        
        collectionView.register(nibName, forCellWithReuseIdentifier: reuseIdentifierNumPadCell)
        
        let numItem = 3
        let widthArea = numPadView.bounds.size.width - 2
        let heightArea = (numPadView.bounds.size.height/5)*4 - 4
        let widthItem = floor(widthArea / CGFloat(numItem))
        let heightItem = heightArea / 4
        print("widthArea \(widthArea)")
        print("widthItem \(widthItem)")
        print("heightItem \(heightItem)")
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 1
        layout.sectionInset = UIEdgeInsets(top: 1, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: widthItem, height: heightItem)
        collectionView.collectionViewLayout = layout
        setupViews()
    }
    
    
    // MARK: Actions
    
    @IBAction func clearButtonTapped(_ sender: UIButton) {
        
        numTitleLabel.text = "000"
        number = 0
        print("number: \(number)")
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierNumPadCell, for: indexPath) as! SKNumPadCell

        cell.setupWithData(data: arrNumPad[indexPath.item])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let data = arrNumPad[indexPath.item]
        
        switch data {
        case "backspace":
            print(data)
            
            if number != 0 {
                
                number = Int(number / 10)
                
                
                if number < 10  && number > 0 {
                    numTitleLabel.text = "00" + "\(number)"
                } else if number < 100 && number > 9 {
                    numTitleLabel.text = "0" + "\(number)"
                } else if number == 0 {
                    numTitleLabel.text = "000"
                }
            }
            print("number: \(number)")
            
        case "search":
            
            onSearch?("\(number)")
            self.dismiss(animated: true, completion: nil)
            
        case "1", "2", "3", "4", "5", "6", "7", "8", "9", "0":
            
            
            guard let num = Int(data) else {return}
            
            if number < 99 {
                number = number * 10 + num
                
                if number < 10 {
                    numTitleLabel.text = "00" + "\(number)"
                } else if number < 100 && number > 9 {
                    numTitleLabel.text = "0" + "\(number)"
                } else if number < 1000 && number > 99 {
                    numTitleLabel.text = "\(number)"
                }
            }
            print("number: \(number)")
            
        default:
            print("default")
        }
    }
    
    private func setupViews(){
        
        numTitleLabel.text = "00" + "\(number)"
        
        backButton.setRoundButton(cornerRadius: 28)
        backButton.setRoundShadow(shadowRadius: 3.0, shadowOpacity: 0.8)
        
        clearButton.setRoundButton(cornerRadius: 12)
    }
}
