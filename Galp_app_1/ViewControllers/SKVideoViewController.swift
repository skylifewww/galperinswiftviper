//
//  SKVideoViewController.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 30.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import AVFoundation

class SKVideoViewController: UIViewController {
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var volumeOffButton: UIButton!
    @IBOutlet weak var currentTimeLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var timeLineSlider: SKVideoTimeSlider!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var videoView: UIView!
    
    var creation:SKCreationModel? {
        didSet {
            if let creation = creation {
                videoURL = URL(string: creation.videoURL!)
                avPlayer = AVPlayer(url: videoURL)
                avPlayerLayer = AVPlayerLayer(player: avPlayer)
                avPlayerLayer.videoGravity = .resizeAspectFill
                urlImage =  URL(string: creation.imageURL!)
                titleName = creation.name
            }
        }
    }
    
    //MARK: Variables
    var avPlayer: AVPlayer!
    var avPlayerLayer: AVPlayerLayer!
    var isPlaying = Bool()
    var videoURL: URL!
    var urlImage: URL!
    var titleName:String?
    var timeObserver: Any!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        videoView.layer.addSublayer(avPlayerLayer)
        avPlayer.currentItem?.addObserver(self, forKeyPath: "duration", options: [.new, .initial], context: nil)
        addTimeObserver()
        avPlayer.isMuted = false
        isPlaying = false
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear")
        avPlayer.currentItem?.removeObserver(self, forKeyPath: "duration")
        avPlayerLayer.removeFromSuperlayer()
        avPlayer.removeTimeObserver(timeObserver)
        avPlayer = nil
        timeObserver = nil
    }

    override func viewDidLayoutSubviews(){
        super.viewDidLayoutSubviews()
        avPlayerLayer.frame = videoView.bounds
    }
    
    @IBAction func playButtonTapped(_ sender: UIButton) {
        
        if isPlaying {
            
        } else {
            avPlayer.play()
        }
        isPlaying = true
        
        playButton.isEnabled = false;
        pauseButton.isEnabled = true;
    }
    
    @IBAction func pauseButtonTapped(_ sender: UIButton) {
        
        if isPlaying {
            avPlayer.pause()
        } else {
            avPlayer.play()
        }
        isPlaying = !isPlaying
    }
    
    @IBAction func volumeOffButtonTapped(_ sender: UIButton) {
        
        avPlayer.isMuted = !avPlayer.isMuted
        
        if avPlayer.isMuted {
            
            volumeOffButton.setImage(#imageLiteral(resourceName: "volume_down"), for: UIControlState.normal)
        } else {
            volumeOffButton.setImage(#imageLiteral(resourceName: "volume_off"), for: UIControlState.normal)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }

    
    @IBAction func timeLineSlidervalueDidChahge(_ sender: UISlider) {
        
        avPlayer.seek(to: CMTimeMake(Int64(sender.value*1000), 1000))
    }
    
    func addTimeObserver() {
        let interval = CMTime(seconds: 0.5, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        let mainQueue = DispatchQueue.main
        self.timeObserver = avPlayer.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue, using: {[weak self] time in
            guard let currentItem = self?.avPlayer.currentItem else {return}
            self?.timeLineSlider.maximumValue = Float(currentItem.duration.seconds)
            self?.timeLineSlider.value = Float(currentItem.currentTime().seconds)
            self?.currentTimeLabel.text = self?.getTimeString(from: currentItem.currentTime())
            
        })
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "duration", let duration = avPlayer.currentItem?.duration.seconds, duration > 0.0 {
            self.durationLabel.text = getTimeString(from: avPlayer.currentItem!.duration)
        }
    }
    
    func getTimeString(from time:CMTime) -> String {

        let totalSeconds = CMTimeGetSeconds(time)
        
        if totalSeconds == 0 || totalSeconds.isNaN
        {
            return "00:00"
        }
        
        let hours = Int(totalSeconds/3600)
        let minutes = Int(totalSeconds/60) % 60
        let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        
        if hours > 0 {
            return String(format: "%02i:%02i:%02i", arguments: [hours, minutes, seconds])
        } else {
            return String(format: "%02i:%02i", arguments: [minutes, seconds])
        }
    }
    
    private func setupViews(){
        let mainColor = #colorLiteral(red: 0.5095188618, green: 0.364217639, blue: 0.3141612709, alpha: 1)
        
        if let titleName = titleName {
            titleLabel.text = titleName
        }
        
        bottomView.setupGradient(topColor: UIColor.black.withAlphaComponent(0.0), bottomtColor: UIColor.black.withAlphaComponent(0.5))
        topView.setupGradient(topColor: UIColor.black.withAlphaComponent(0.5), bottomtColor: UIColor.black.withAlphaComponent(0.0))

//        let titleTextAttributed: [NSAttributedStringKey: Any] = [.foregroundColor: UIColor.white, .font: UIFont(name: "Phenomena-Black", size: 24) as Any]
//        navigationController?.navigationBar.titleTextAttributes = titleTextAttributed
//        
//        let volumeOff = UIButton(type: UIButtonType.custom)
//        volumeOff.setImage(#imageLiteral(resourceName: "volume_off"), for: UIControlState.normal)
//        volumeOff.frame = CGRect(x: 0, y: 0, width: 42, height: 42)
//        volumeOff.imageEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
////        volumeOff.addTarget(self, action:  #selector(volumeOffButtonAction), for: UIControlEvents.touchUpInside)
//        volumeOff.backgroundColor = .white
//        volumeOff.setRoundBorderEdgeButton(cornerRadius: 21, borderWidth: 1, borderColor: mainColor)
//        let rightButton = UIBarButtonItem(customView: volumeOff)
//        self.navigationItem.rightBarButtonItem = rightButton;
//        
//        let backButton = UIButton(type: UIButtonType.system)
//        backButton.setImage(#imageLiteral(resourceName: "back_white"), for: UIControlState.normal)
//        backButton.tintColor = .white
//        backButton.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
//        backButton.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
//        backButton.addTarget(self, action:  #selector(backButtonAction), for: UIControlEvents.touchUpInside)
//        let leftButton = UIBarButtonItem(customView: backButton)
//        self.navigationItem.leftBarButtonItem = leftButton;
        

        playButton.isEnabled = true;
        pauseButton.isEnabled = false;
        
        playButton.backgroundColor = .white
        playButton.setRoundBorderEdgeButton(cornerRadius: 32, borderWidth: 3, borderColor: mainColor)
        
        pauseButton.backgroundColor = .white
        pauseButton.setRoundBorderEdgeButton(cornerRadius: 32, borderWidth: 3, borderColor: mainColor)
        
        volumeOffButton.backgroundColor = .white
        volumeOffButton.setRoundBorderEdgeButton(cornerRadius: 21, borderWidth: 1, borderColor: mainColor)
    }
}
