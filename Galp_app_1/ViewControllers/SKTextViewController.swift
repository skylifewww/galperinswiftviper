//
//  SKTextViewController.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 27.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class SKTextViewController: UIViewController {
    
    var сreation: SKCreationModel? {
        didSet {
            if let сreation = сreation, let imageURL = сreation.imageURL {
                
                urlImage =  URL(string: imageURL)
                titleName = сreation.name
            }
        }
    }

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var creationImage: UIImageView!
    @IBOutlet weak var creationLabel: UILabel!
    @IBOutlet weak var descriptLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    //MARK: Variables
    var urlImage: URL!
    var titleName:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupViews()
    }

    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setupViews() {
        
        if let urlImage = urlImage {
            creationImage.sd_setImage(with: urlImage, completed: { (img, error, type, urls) in
                self.creationImage.image = img
            })
        }
        
        if let titleName = titleName {
            titleLabel.text = titleName
            creationLabel.text = titleName
        }
        
        if let number = сreation?.number {

            guard let intNum = Int(number) else {return}
            if intNum < 10 {
                idLabel.text = "00\(intNum)"
            } else if intNum < 100 && intNum > 9 {
                idLabel.text = "0\(intNum)"
            } else {
                idLabel.text = "\(intNum)"
            }
        }
        
        if let description = сreation?.description {
            textLabel.text = description
        }

        

        idLabel.setRoundBorderEdgeLabel(cornerRadius: 21, borderWidth: 1, borderColor: #colorLiteral(red: 0.5095188618, green: 0.364217639, blue: 0.3141612709, alpha: 1))
    }

}
