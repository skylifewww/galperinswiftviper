//
//  SKEnterViewController.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 24.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class SKEnterViewController: UIViewController {

    @IBOutlet weak var btAdult: UIButton!
    @IBOutlet weak var btFamily: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    @IBAction func btAdultTapped(_ sender: UIButton) {
        
        print("btAdultTapped")
    }
    
    @IBAction func btFamilyTapped(_ sender: Any) {
        
        let mainViewController = self.storyboard?.instantiateViewController(withIdentifier: "SKCreationsViewController") as! SKCreationsViewController
        self.present(mainViewController, animated: true, completion: nil)
        
        print("btFamilyTapped")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   private func setupViews()
   {
    let mainColor = #colorLiteral(red: 0.8066058755, green: 0.7002419233, blue: 0.5360015631, alpha: 1)
    
    btAdult.backgroundColor = .clear
    btAdult.setRoundBorderEdgeButton(cornerRadius: 20, borderWidth: 5, borderColor: mainColor)
    
    btFamily.backgroundColor = .clear
    btFamily.setRoundBorderEdgeButton(cornerRadius: 20, borderWidth: 5, borderColor: mainColor)
  }
}

