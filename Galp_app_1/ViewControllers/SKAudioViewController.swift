//
//  SKAudioViewController.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 28.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
//import KDEAudioPlayer
import SDWebImage
import AVFoundation

class SKAudioViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var creationImage: UIImageView!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var finishTimeLabel: UILabel!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var pauseButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var volumeSlider: SKVideoTimeSlider!
    @IBOutlet weak var timelineSlider: SKAudioTimeSlider!
    @IBOutlet weak var volumeDownImage: UIImageView!
    @IBOutlet weak var volumeUpImage: UIImageView!
    
    var creation:SKCreationModel? {
        didSet {
            if let creation = creation {
                audioURL = URL(string: creation.audioURL!)
                avPlayer = AVPlayer(url: audioURL)
                urlImage =  URL(string: creation.imageURL!)
                titleName = creation.name
            }
        }
    }

    
    //MARK: Variables
    var avPlayer: AVPlayer!
    var titleName:String?
    var isPlaying = Bool()
    var audioURL: URL!
    var urlImage: URL!
    var timeObserver: Any!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        isPlaying = false
        
        avPlayer.currentItem?.addObserver(self, forKeyPath: "duration", options: [.new, .initial], context: nil)
        addTimeObserver()
        timelineSlider?.isContinuous = true
        setupViews()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print("viewWillDisappear")
        avPlayer.currentItem?.removeObserver(self, forKeyPath: "duration")
        avPlayer.removeTimeObserver(timeObserver)
        avPlayer = nil
        timeObserver = nil
    }
    
    func addTimeObserver() {
        let interval = CMTime(seconds: 0.5, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        let mainQueue = DispatchQueue.main
        self.timeObserver = avPlayer.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue, using: {[weak self] time in
            guard let currentItem = self?.avPlayer.currentItem else {return}
            self?.timelineSlider.maximumValue = Float(currentItem.duration.seconds)
            self?.timelineSlider.value = Float(currentItem.currentTime().seconds)
            self?.startTimeLabel.text = self?.getTimeString(from: currentItem.currentTime())
            
        })
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "duration", let duration = avPlayer.currentItem?.duration.seconds, duration > 0.0 {
            self.finishTimeLabel.text = getTimeString(from: avPlayer.currentItem!.duration)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func playButtonTapped(_ sender: UIButton) {

        if isPlaying {
            
        } else {
            avPlayer.play()
        }
        isPlaying = true
        playButton.isEnabled = false;
        stopButton.isEnabled = true;
        pauseButton.isEnabled = true;
    }
    
    @IBAction func pauseButtonTapped(_ sender: UIButton) {
        
        if isPlaying {
            avPlayer.pause()
        } else {
            avPlayer.play()
        }
        isPlaying = !isPlaying
    }
    
    @IBAction func stopButtonTapped(_ sender: UIButton) {
        
        avPlayer.pause()
        isPlaying = false
        playButton.isEnabled = true;
    }

    @IBAction func timelineSliderValueChanged(_ sender: UISlider) {
        avPlayer.seek(to: CMTimeMake(Int64(sender.value*1000), 1000))
    }
    
    
    @IBAction func volumeSliderValueChanged(_ sender: UISlider) {
        avPlayer.volume = sender.value
    }

    private func setupViews(){
        
        let mainColor = #colorLiteral(red: 0.5095188618, green: 0.364217639, blue: 0.3141612709, alpha: 1)

        if let urlImage = urlImage {
            creationImage.sd_setImage(with: urlImage, completed: { (img, error, type, urls) in
                self.creationImage.image = img
            })
        }
        
        if let titleName = titleName {
            titleLabel.text = titleName
        }
        
        playButton.isEnabled = true;
        stopButton.isEnabled = false;
        pauseButton.isEnabled = false;

        playButton.backgroundColor = .clear
        playButton.setRoundBorderEdgeButton(cornerRadius: 32, borderWidth: 3, borderColor: mainColor)
        
        pauseButton.backgroundColor = .clear
        pauseButton.setRoundBorderEdgeButton(cornerRadius: 32, borderWidth: 3, borderColor: mainColor)
        
        stopButton.backgroundColor = .clear
        stopButton.setRoundBorderEdgeButton(cornerRadius: 32, borderWidth: 3, borderColor: mainColor)
        
    }
    
    //MARK: Convert Time to String
    func getTimeString(from time:CMTime) -> String {
        
        let totalSeconds = CMTimeGetSeconds(time)
        
        if totalSeconds == 0 || totalSeconds.isNaN
        {
            return "00:00"
        }
        
        let hours = Int(totalSeconds/3600)
        let minutes = Int(totalSeconds/60) % 60
        let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
        
        if hours > 0 {
            return String(format: "%02i:%02i:%02i", arguments: [hours, minutes, seconds])
        } else {
            return String(format: "%02i:%02i", arguments: [minutes, seconds])
        }
    }
}
