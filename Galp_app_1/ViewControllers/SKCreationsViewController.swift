//
//  SKCreationsViewController.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 04.05.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

private let reuseIdentifierCell = "SKCreatCell"

class SKCreationsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numpadButton: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    
    
    fileprivate var creationsFeed = SKCreationFeedModel(list: .family, type: .imagesAndMasks)
    
    var creations:[SKCreationModel] = []
    var currentCreations:[SKCreationModel] = []
    fileprivate var isSearch: Bool = false
    let headerHeight: CGFloat = 64
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getFeed()
        
        let nibName = UINib(nibName: reuseIdentifierCell, bundle: nil)
        
        collectionView?.register(nibName, forCellWithReuseIdentifier: reuseIdentifierCell)

        
        let numItem = 4
        let widthArea = view.bounds.size.width - 53 - CGFloat((numItem - 1) * 30)
        let widthItem = floor(widthArea / CGFloat(numItem))
        let heightItem = widthItem * 1.2
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 8
        layout.scrollDirection = .vertical
        layout.sectionInset = UIEdgeInsets(top: 25, left: 25, bottom: 25, right: 28)
        layout.itemSize = CGSize(width: widthItem, height: heightItem)
        self.collectionView?.collectionViewLayout = layout
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: Feeds
    
    func getFeed() {
        
        creationsFeed.clearFeed()
        creationsFeed.requestAll(comletionHandler: { (succeed, _, error) -> Void in
            if succeed {
                if self.creationsFeed.creationsArray.count > 0{
                    
                    self.creations = self.creationsFeed.creationsArray
                    self.currentCreations = self.creations
                    self.creationsFeed.clearFeed()
                    self.isSearch = false
                    self.collectionView?.reloadData()
                }

            } else if let error = error {
                let alert = self.alertControllerWithMessage(error)
                self.present(alert, animated: true, completion: nil)
            }
        })
    }
    
    
    // MARK: UICollectionViewDataSource
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return currentCreations.count > 0 ? currentCreations.count : 0
    }
    
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifierCell, for: indexPath) as! SKCreatCell
        
        cell.сreation = currentCreations[indexPath.item]
        
        return cell
    }
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if creations[indexPath.item].videoURL != nil {
            let videoViewController = self.storyboard?.instantiateViewController(withIdentifier: "SKVideoViewController") as! SKVideoViewController
            videoViewController.creation = currentCreations[indexPath.item]
            self.present(videoViewController, animated: true, completion: nil)
        } else if creations[indexPath.item].audioURL != nil {
            let audioViewController = self.storyboard?.instantiateViewController(withIdentifier: "SKAudioViewController") as! SKAudioViewController
            audioViewController.creation = currentCreations[indexPath.item]
            self.present(audioViewController, animated: true, completion: nil)
            
        } else if creations[indexPath.item].description != nil {
            let textViewController = self.storyboard?.instantiateViewController(withIdentifier: "SKTextViewController") as! SKTextViewController
            textViewController.сreation = currentCreations[indexPath.item]
            self.present(textViewController, animated: true, completion: nil)
        }
    }
    
    
    // Search By Numpad
    
    func searchByNumPad(searchNumber: String) {
        currentCreations = creations.filter({ creation -> Bool in
            
            if searchNumber.isEmpty { return true }
            return creation.number!.lowercased().contains(searchNumber.lowercased())
            
        })
        collectionView?.reloadData()
    }

    
    // Search Bar Delegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        currentCreations = creations.filter({ creation -> Bool in
            
            if searchText.isEmpty { return true }
            return creation.name.lowercased().contains(searchText.lowercased())
            
        })
        collectionView?.reloadData()
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
 
            currentCreations = creations
      
        collectionView?.reloadData()
    }
    
    
    // Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "NumPad" {
            let numPad = segue.destination as! SKNumPadViewController
            numPad.onSearch = { (data) in
                
                if data == "0" {
                    self.currentCreations = self.creations
                    self.collectionView?.reloadData()
                } else {
                    self.currentCreations = self.creations.filter({ creation -> Bool in
                        
                        if data.isEmpty { return true }
                        
                        return creation.number!.lowercased().contains(data.lowercased())
                        
                    })
                    self.collectionView?.reloadData()
                }
            }
        }
    }
}
