//
//  SKCreation+CoreDataProperties.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 24.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//
//

import Foundation
import CoreData


extension SKCreation {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<SKCreation> {
        return NSFetchRequest<SKCreation>(entityName: "SKCreation")
    }

    @NSManaged public var audioUrl: String?
    @NSManaged public var dateCreation: NSDate?
    @NSManaged public var descriptCreation: String?
    @NSManaged public var id: Int64
    @NSManaged public var imageUrl: String?
    @NSManaged public var name: String?
    @NSManaged public var videoUrl: String?

}
