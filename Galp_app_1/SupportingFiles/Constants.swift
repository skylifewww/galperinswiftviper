//
//  Constants.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 24.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

struct Constants {
    
    static let baseURL = "https://store.yuliygalperin4u.com/api/exhibits/list/?"
    static let storageURL = "https://store.yuliygalperin4u.com/exhibit/"
    
    static let cellPadding: CGFloat = 5.0
    static let screenHeight: CGFloat = UIScreen.main.bounds.height
    static let screenWidth: CGFloat = UIScreen.main.bounds.width

    static let mainColor = UIColor.init(red: 0.194, green: 0.164, blue: 0.118, alpha: 1.0)
    
    static let noInternetOverlayTag: Int = 2000789
//    static let dateTimeFormat = "yyyy-MM-dd HH:mm:ss"
//    static let nonTrendedDateTimeFormat = "0000-00-00 00:00:00"
    
    // adjust here
    static let searchResultsLimit: Int = 10
    static let preferredSearchRating = "pg"
    static let preferredImageType = "fixed_width_downsampled"
    static let fullImageType = "downsized_medium"
    static let trendedIconName = "trendedIcon"
    static let trendedIconSize: CGFloat = 15
    
}
