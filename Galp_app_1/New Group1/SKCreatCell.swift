//
//  SKCreatCell.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 25.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit
import SDWebImage

class SKCreatCell: UICollectionViewCell {
    
    var сreation: SKCreationModel? {
        didSet {
            if let сreation = сreation, let imageURL = сreation.imageURL {
                
                imageCreation.sd_setImage(with: URL.init(string: imageURL))
                titleLabel.text = сreation.name
                idLabel.text = сreation.number
                guard let intNum = Int(сreation.number!) else {return}
                if intNum < 10 {
                    idLabel.text = "00\(intNum)"
                } else if intNum < 100 && intNum > 9 {
                    idLabel.text = "0\(intNum)"
                } else {
                    idLabel.text = "\(intNum)"
                }
                
                if let _ = сreation.videoURL{
                    print("сreation.videoURL")
                    videoImage.alpha = 1
                } else {
                    print("not сreation.videoURL")
                    videoImage.alpha = 0.5
                }
                if let _ = сreation.audioURL{
                    print("audioURL")
                    audioImage.alpha = 1
                } else {
                    print("not audioURL")
                    audioImage.alpha = 0.5
                }
                if (сreation.description?.count)! > 0 {
                    print("description")
                    textImage.alpha = 1
                } else {
                    print("not description")
                    textImage.alpha = 0.5
                }
            }
            
        }
    }
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imageCreation: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptLabel: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var videoView: UIView!
    @IBOutlet weak var videoImage: UIImageView!
    @IBOutlet weak var audioView: UIView!
    @IBOutlet weak var audioImage: UIImageView!
    @IBOutlet weak var textView: UIView!
    @IBOutlet weak var textImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupViews()
    }
    
    private func setupViews() {
   
        idLabel.setRoundBorderEdgeLabel(cornerRadius: 21, borderWidth: 1, borderColor: #colorLiteral(red: 0.5095188618, green: 0.364217639, blue: 0.3141612709, alpha: 1))
        videoView.setRoundBorderEdgeView(cornerRadius: 21, borderWidth: 1, borderColor: #colorLiteral(red: 0.5095188618, green: 0.364217639, blue: 0.3141612709, alpha: 1))
        audioView.setRoundBorderEdgeView(cornerRadius: 21, borderWidth: 1, borderColor: #colorLiteral(red: 0.5095188618, green: 0.364217639, blue: 0.3141612709, alpha: 1))
        textView.setRoundBorderEdgeView(cornerRadius: 21, borderWidth: 1, borderColor: #colorLiteral(red: 0.5095188618, green: 0.364217639, blue: 0.3141612709, alpha: 1))
    }
}
