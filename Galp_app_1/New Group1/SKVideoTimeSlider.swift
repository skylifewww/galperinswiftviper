//
//  SKVideoTimeSlider.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 01.05.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

@IBDesignable
class SKVideoTimeSlider: UISlider {
    
    @IBInspectable var trackHeight: CGFloat = 6
    
    @IBInspectable var thumbImage: UIImage? {
        didSet {
            setThumbImage(thumbImage, for: .normal)
        }
    }
    
    @IBInspectable var thumbHighlightedImage: UIImage? {
        didSet {
            setThumbImage(thumbHighlightedImage, for: .highlighted)
        }
    }
    
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        var newRect = super.trackRect(forBounds: bounds)
        newRect.size.height = trackHeight
        return newRect
    }
}
