//
//  SKCreationCollectionViewLayout.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 24.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import Foundation
import UIKit

protocol SKCreationCollectionViewLayoutDelegate {

    func collectionView(_ collectionView:UICollectionView, heightForCreationAtIndexPath indexPath:IndexPath, fixedWidth:CGFloat) -> CGFloat
}

class SKCreationLayoutAttributes: UICollectionViewLayoutAttributes {
    // custom attributes
    var creationHeight: CGFloat = 0.0
    var creationWidth: CGFloat = 0.0
    
    override func copy(with zone: NSZone?) -> Any {
        let copy = super.copy(with: zone) as! SKCreationLayoutAttributes
        copy.creationHeight = creationHeight
        copy.creationWidth = creationWidth
        return copy
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let attributes = object as? SKCreationLayoutAttributes {
            if(attributes.creationHeight == creationHeight && attributes.creationWidth == creationWidth) {
                return super.isEqual(object)
            }
        }
        return false
    }
    
}

class SKCreationCollectionViewLayout: UICollectionViewLayout {
    
    var delegate: SKCreationCollectionViewLayoutDelegate!
    fileprivate var attributes = [SKCreationLayoutAttributes]()
    fileprivate var contentHeight: CGFloat = 0.0
    fileprivate var contentWidth: CGFloat = 0.0
    
    override class var layoutAttributesClass : AnyClass {
        return SKCreationLayoutAttributes.self
    }
    
    override func prepare() {
        
//        var column = 0
        contentHeight = 0
        contentWidth = collectionView!.frame.width - collectionView!.contentInset.left - collectionView!.contentInset.right
        let itemWidth: CGFloat = floor(contentWidth / 4.0)
//        let xOffset: [CGFloat] = [0, itemWidth]
//        var yOffset: [CGFloat] = [0, 0]
        attributes = []
        
        for item in 0..<collectionView!.numberOfItems(inSection: 0) {
            
            let indexPath = IndexPath.init(item: item, section: 0)
            let creationWidth = itemWidth - (2 * Constants.cellPadding)
            let creationHeight = creationWidth * 0.184
//            let creationHeight = delegate.collectionView(collectionView!, heightForCreationAtIndexPath: indexPath, fixedWidth: creationWidth)
            _ = creationHeight + (2 * Constants.cellPadding)
            
//            if yOffset[0] > yOffset[1] {
//                column = 1
//            } else {
//                column = 0
//            }
            
//            let itemFrame = CGRect(x: xOffset[column], y: yOffset[column], width: itemWidth, height: itemHeight)
            let attribute = SKCreationLayoutAttributes.init(forCellWith: indexPath)
//            attribute.frame = itemFrame
            attribute.creationHeight = creationHeight
            attribute.creationWidth = creationWidth
            attributes.append(attribute)
            
            // update content height and y offset
//            contentHeight = max(contentHeight, itemFrame.maxY)
//            yOffset[column] = yOffset[column] + itemHeight
            
        }
        
    }
    
    override var collectionViewContentSize : CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var layoutAttributes = [UICollectionViewLayoutAttributes]()
        for attribute in attributes {
            if attribute.frame.intersects(rect) {
                layoutAttributes.append(attribute)
            }
        }
        return layoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return attributes[indexPath.item]
    }
    
}
