//
//  SKNumPadCell.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 05.05.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import UIKit

class SKNumPadCell: UICollectionViewCell {
    @IBOutlet weak var imageCell: UIImageView!
    @IBOutlet weak var numLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setupWithData(data: String){
        
        if data == "search" || data == "backspace" {
            numLabel.isHidden = true
            imageCell.isHidden = false
            imageCell.image = UIImage(named: data)
            self.backgroundColor = #colorLiteral(red: 0.8066058755, green: 0.7002419233, blue: 0.5360015631, alpha: 1)
            
        } else {
            
            numLabel.isHidden = false
            imageCell.isHidden = true
            numLabel.text = data
            self.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        }
    }

}
