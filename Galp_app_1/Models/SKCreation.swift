//
//  SKCreationModel.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 24.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import Foundation
import SwiftyJSON

class SKCreationModel {
    
    var width: CGFloat = 0
    var height: CGFloat = 0
    var url: String?
    var widthFull: CGFloat = 0
    var heightFull: CGFloat = 0
    var urlFull: String?
    var id: String?
    var rating: String?
    var trended: Bool?
    
    convenience init(data: JSON) {
        self.init()
        
        if let gifId = data["id"].string {
            id = gifId
        }
        if let gifRating = data["rating"].string {
            rating = gifRating
        }
        if let gifURL = data["images"][Constants.preferredImageType]["url"].string {
            url = gifURL
        }
        if let gifWidth = data["images"][Constants.preferredImageType]["width"].string {
            if let w = Double(gifWidth) {
                width = CGFloat(w)
            }
        }
        if let gifHeight = data["images"][Constants.preferredImageType]["height"].string {
            if let h = Double(gifHeight) {
                height = CGFloat(h)
            }
        }
        if let gifURL = data["images"][Constants.fullImageType]["url"].string {
            urlFull = gifURL
        }
        if let gifWidth = data["images"][Constants.fullImageType]["width"].string {
            if let w = Double(gifWidth) {
                widthFull = CGFloat(w)
            }
        }
        if let gifHeight = data["images"][Constants.fullImageType]["height"].string {
            if let h = Double(gifHeight) {
                heightFull = CGFloat(h)
            }
        }
        if let trendingDateTime = data["trending_datetime"].string {
            
            // To determine whether a gif has ever trended, check the string Or if the date is valid
//            (trendingDateTime == Constants.nonTrendedDateTimeFormat) ? (trended = false) : (trended = true)
            
            //            let dateFormatter = NSDateFormatter()
            //            dateFormatter.dateFormat = Constants.dateTimeFormat
            //            let date = dateFormatter.dateFromString(trendingDateTime)
            
        }
        
    }
    
}
