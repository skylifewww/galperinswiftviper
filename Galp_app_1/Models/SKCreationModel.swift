//
//  SKCreationModel.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 24.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//


import Foundation
import SwiftyJSON

class SKCreationModel {
    
    var id: String?
    var list: Int = 0
    var type: Int = 0
    var name: String = ""
    var number: String?
    var description: String?
    var shortDescription: String?
    var imageURL: String?
    var videoURL: String?
    var audioURL: String?
    
    convenience init(data: JSON) {
        self.init()
        
        if let creationID = data["_id"].string {
            id = creationID
        }
        if let creationNumber = data["number"].string {
                number = creationNumber
        }
        if let creationType = data["type"].string {
            if let t = Int(creationType) {
                type = Int(t)
            }
        }
        if let creationList = data["list"].string {
            if let l = Int(creationList) {
                list = Int(l)
            }
        }
        if let creationName = data["name"].string {
            name = creationName
        }
        if let creationDescription = data["description"].string {
            description = creationDescription
        }
        if let creationShortDescription = data["shortDescription"].string {
            shortDescription = creationShortDescription
        }
        if let creationImage = data["image"].string {
            imageURL = Constants.storageURL + creationImage
        }
        if let creationVideo = data["video"].string {
            videoURL = Constants.storageURL + creationVideo
        }
        if let creationAudio = data["audio"].string {
            audioURL = Constants.storageURL + creationAudio
        }
    }
}
