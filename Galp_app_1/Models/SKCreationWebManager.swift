//
//  SKCreationWebManager.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 28.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class SKCreationWebManager {

    static let sharedInstance = SKCreationWebManager()
    fileprivate var alamofireManager: Alamofire.SessionManager!
    
    fileprivate init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30 // seconds
        configuration.timeoutIntervalForResource = 30 // seconds
        alamofireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    func queryCreationsWithTypeAndList(list:String, type:String, completionHandler:@escaping (_ creations: [SKCreationModel]?, _ error: String?) -> Void) {
        
        alamofireManager.request(Constants.baseURL, method: .get, parameters: ["list": list, "type": type], encoding: URLEncoding.default).responseJSON(completionHandler: { response in
            
            switch response.result {
            case .success(let result):
 
                let resultJSON = JSON.init(result)
                if let creationsData = resultJSON["result"].array {
                    var creations = [SKCreationModel]()
                    for creationJSON in creationsData {
                        let creation = SKCreationModel.init(data: creationJSON)
                        creations.append(creation)
                    }
                    completionHandler(creations, nil)
                } else {
                    completionHandler(nil, "Something is wrong")
                }
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        })
    }
    
    func queryAllCreations(completionHandler:@escaping (_ creations: [SKCreationModel]?, _ error: String?) -> Void) {
        
        alamofireManager.request(Constants.baseURL, method: .get, parameters: [:], encoding: URLEncoding.default).responseJSON(completionHandler: { response in
            
            switch response.result {
            case .success(let result):
                let resultJSON = JSON.init(result)
                if let creationsData = resultJSON["result"].array {
                    var creations = [SKCreationModel]()
                    for creationJSON in creationsData {
                        let creation = SKCreationModel.init(data: creationJSON)
                        creations.append(creation)
                    }
                    completionHandler(creations, nil)
                } else {
                    completionHandler(nil, "Something is wrong")
                }
            case .failure(let error):
                completionHandler(nil, error.localizedDescription)
            }
        })
    }
}
