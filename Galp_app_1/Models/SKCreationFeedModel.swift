//
//  SKCreationFeedModel.swift
//  Galp_app_1
//
//  Created by Vladimir Nybozhinsky on 28.04.2018.
//  Copyright © 2018 Vladimir Nybozhinsky. All rights reserved.
//

import Foundation
import UIKit

enum FeedList:String {
    case adult = "1"
    case family = "0"
}

enum FeedType:String {
    case biblicalDreams = "0"
    case art = "1"
    case windInTheMane = "2"
    case landscapesAndStillLifes = "3"
    case imagesAndMasks = "4"
    case aestheticErotica = "5"
}

class SKCreationFeedModel {
    
    
    //        fileprivate var maxgifs = Constants.searchResultsLimit
    //        var currentOffset = 0
    //        fileprivate var previousOffset = -1
    var creationsArray = [SKCreationModel]()
    fileprivate var requesting: Bool = false
    fileprivate var type: FeedType = .biblicalDreams
    fileprivate var list: FeedList = .family
    
//    enum FeedList:String {
//        case adult = "1"
//        case family = "0"
//    }
//
//    enum FeedType:String {
//        case biblicalDreams = "0"
//        case art = "1"
//        case windInTheMane = "2"
//        case landscapesAndStillLifes = "3"
//        case imagesAndMasks = "4"
//        case aestheticErotica = "5"
//    }
    
    
    init(list: FeedList,type: FeedType) {
        self.type = type
        self.list = list
    }
    
    func clearFeed() {
        creationsArray = []
        requesting = false
    }
    
        func requestWithTypeAndList(comletionHandler:@escaping (_ succeed: Bool, _ total: Int?, _ error: String?) -> Void) {
            if requesting {
                comletionHandler(false, nil, nil)
                return
            }

            print("requestFeed")
            requesting = true
                
                SKCreationWebManager.sharedInstance.queryCreationsWithTypeAndList(list: self.list.rawValue, type: self.type.rawValue, completionHandler: {(creations, error) -> Void in
                   
                    self.requesting = false
                    if let creations = creations {

                        print("creations \(creations)")
                        self.creationsArray.append(contentsOf: creations)
                        comletionHandler(true, creations.count, nil)
                    } else {
                        comletionHandler(false, nil, error)
                    }
                })
        }
    
    func requestAll(comletionHandler:@escaping (_ succeed: Bool, _ total: Int?, _ error: String?) -> Void) {
        if requesting {
            comletionHandler(false, nil, nil)
            return
        }
        
        print("requestFeed")
        requesting = true
        
        SKCreationWebManager.sharedInstance.queryAllCreations { (creations, error) -> Void in
            
            self.requesting = false
            if let creations = creations {
                
                print("creations \(creations)")
                self.creationsArray.append(contentsOf: creations)
                comletionHandler(true, creations.count, nil)
            } else {
                comletionHandler(false, nil, error)
            }
        }
    }
}

